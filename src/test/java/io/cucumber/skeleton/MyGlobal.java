package io.cucumber.skeleton;

import org.openqa.selenium.WebDriver;

public class MyGlobal {
    WebDriver driver = null;

    private static MyGlobal ourInstance = new MyGlobal();

    public static MyGlobal getInstance() {
        return ourInstance;
    }

    private MyGlobal() {
        System.setProperty("webdriver.gecko.driver", "/home/student/home/selenium_drivers/geckodriver_0.24.0_linux_64");
    }
}
