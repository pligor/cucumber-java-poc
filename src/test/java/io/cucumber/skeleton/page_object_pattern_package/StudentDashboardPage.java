package io.cucumber.skeleton.page_object_pattern_package;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class StudentDashboardPage extends BasePage<StudentPageObjectRepo> {
    public StudentDashboardPage(WebDriver driver) {
        super(driver, new StudentPageObjectRepo(driver));
    }

    public void verify_that_we_are_here() {
        Assert.assertTrue(
                objectRepository.getHtml().getText().contains("My Dashboard")
        );
    }
}
