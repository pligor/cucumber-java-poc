package io.cucumber.skeleton.page_object_pattern_package;

import org.openqa.selenium.WebDriver;

public class BasePage<T extends BasePageObjectRepository> {
    public WebDriver driver;
    public T objectRepository;

    public BasePage(WebDriver driver, T objectRepository) {
        this.driver = driver;
        this.objectRepository = objectRepository;
    }
}
