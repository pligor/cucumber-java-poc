package io.cucumber.skeleton.page_object_pattern_package;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CoursesPage extends BasePage<CoursePageObjectRepository> {
    public CoursesPage(WebDriver driver) {
        super(driver, new CoursePageObjectRepository(driver));
    }

    public void go_to() {
        driver.get("http://courses.ultimateqa.com");
        //            driver.manage().window().setSize(new Dimension(800, 900));
        driver.manage().window().maximize();
    }

    public LoginPage click_signin() {
        //            WebElement toggle_navbar = driver.findElement(
//                    By.cssSelector("button.header-nav__mobile-btn"));
//
//            ExpectedConditions.visibilityOf(toggle_navbar).apply(driver).click();
//            WebElement toggle_navbar = driver.findElement(
//                    By.cssSelector("button.header-nav__mobile-btn"));

        ExpectedConditions.visibilityOf(objectRepository.getSigninLink()).apply(driver).click();

        return new LoginPage(driver);
    }
}
