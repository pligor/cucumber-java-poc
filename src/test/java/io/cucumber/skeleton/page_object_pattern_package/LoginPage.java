package io.cucumber.skeleton.page_object_pattern_package;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends BasePage<LoginPageObjectRepo> {
    public LoginPage(WebDriver driver) {
        super(driver, new LoginPageObjectRepo(driver));
        Assert.assertTrue(
                objectRepository.getSigninButton().getAttribute("value").contains("Sign in")
        );
    }

    public StudentDashboardPage login(String username, String password) {
        fill_username(username);
        fill_password(password);
        WebElement submit_button = objectRepository.getSigninButton();
        submit_button.click();

        return new StudentDashboardPage(driver);
    }

    private void fill_username(String username) {
        fill_field(username, objectRepository.getUserEmail());
    }

    private void fill_password(String password) {
        fill_field(password, objectRepository.getUserPass());
    }

    private void fill_field(String value, WebElement element) {
        element.click();
        element.clear();
        element.sendKeys(value);
    }
}
