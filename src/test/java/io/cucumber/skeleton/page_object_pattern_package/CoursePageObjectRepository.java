package io.cucumber.skeleton.page_object_pattern_package;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CoursePageObjectRepository extends BasePageObjectRepository {
    public CoursePageObjectRepository(WebDriver driver) {
        super(driver);
    }

    public WebElement getSigninLink() {
        return driver.findElement(By.cssSelector("a.my-account"));
    }
}
