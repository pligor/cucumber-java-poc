package io.cucumber.skeleton.page_object_pattern_package;

import org.openqa.selenium.WebDriver;

public class BasePageObjectRepository {
    public WebDriver driver;

    public BasePageObjectRepository(WebDriver driver) {
        this.driver = driver;
    }
}
