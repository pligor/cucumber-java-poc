package io.cucumber.skeleton.page_object_pattern_package;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPageObjectRepo extends BasePageObjectRepository {
    public LoginPageObjectRepo(WebDriver driver) {
        super(driver);
    }

    public WebElement getUserEmail() {
        return driver.findElement(By.id("user_email"));
    }

    public WebElement getUserPass() {
        return driver.findElement(By.id("user_password"));
    }

    public WebElement getSigninButton() {
        return driver.findElement(By.id("btn-signin"));
    }
}
