package io.cucumber.skeleton.page_object_pattern_package;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class StudentPageObjectRepo extends BasePageObjectRepository {
    public StudentPageObjectRepo(WebDriver driver) {
        super(driver);
    }

    public WebElement getHtml() {
        return driver.findElement(By.tagName("html"));
    }
}
