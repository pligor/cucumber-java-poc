package io.cucumber.skeleton;

import cucumber.api.Scenario;
import cucumber.api.java8.En;
import cucumber.api.java8.HookBody;
import cucumber.runtime.junit.Assertions;
import io.cucumber.skeleton.page_object_pattern_package.CoursesPage;
import io.cucumber.skeleton.page_object_pattern_package.LoginPage;
import io.cucumber.skeleton.page_object_pattern_package.StudentDashboardPage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class PageObjectPattern implements En {
    //The class is being instantiated on every scenario !

    WebDriver driver = MyGlobal.getInstance().driver;
    CoursesPage coursesPage = null;
    LoginPage loginPage = null;
    StudentDashboardPage studentDashboardPage = null;

    public PageObjectPattern() {
        Given("Selenium Driver has been loaded", () -> {
            //This is not working
            if (driver == null) {
                System.out.println("need to create a new driver");

                driver = new FirefoxDriver();
                MyGlobal.getInstance().driver = driver;

                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            } else {
                System.out.println("driver is already available");
            }
        });

        Given("the ultimateqa webpage has been loaded", () -> {
            coursesPage = new CoursesPage(driver);
            coursesPage.go_to();
        });

        When("clicking on sign in", () -> {
            loginPage = coursesPage.click_signin();
        });

        When("filling out and submitting the form", () -> {
            studentDashboardPage = loginPage.login("george@pligor.com", "UJT!Yv2|W85JN_F");
        });

        Then("the logged in page has the expected title", () -> {
            studentDashboardPage.verify_that_we_are_here();
            driver.quit();
        });

        When("filling out the form with the wrong information and submitting it", () -> {
            studentDashboardPage = loginPage.login("wrong@email.com", "wrong_password");
        });

        Then("an error message is being displayed", () -> {
            Assert.assertTrue(
                    driver.findElement(By.id("notifications-error")).getText().contains(
                            "Invalid Email or password")
            );
        });

        After(new String[]{"@After_DriverQuit"}, scenario -> {
            driver.quit();
        });
    }


}
