package io.cucumber.skeleton.dependency_injection;

import cucumber.api.java8.En;
import org.junit.Assert;

//public class CustomerSteps extends WithWorld implements En {
public class CustomerSteps implements En {
    //we do not explicitly instantiate the world class
    //the idea is that we have some state in this common class that has been injected to both
    // customer steps class and to goods steps class
    private World world;

    public CustomerSteps(World world) {
//        super();

        this.world = world;

        When("she return the {word} to the store", (String item_type) -> {
            Assert.assertTrue( world.item.getItem_type().contentEquals(item_type) );
            this.world.store_items = new Item[] { world.item };
        });

        When("she shows her receipt", () -> {
            world.customer.refund(world.item.getPrice());
        });

        Then("she will get {float} refunded", (Float value) -> {
            Assert.assertEquals(world.customer.getWallet(), value);

            Assert.assertEquals(0b1, world.store_items.length);
        });
    }
}
