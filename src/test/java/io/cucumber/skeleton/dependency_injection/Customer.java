package io.cucumber.skeleton.dependency_injection;

public class Customer {
    private String name;
    private Float wallet;

    public Customer(String name) {
        this.name = name;
        this.wallet = (float) 0;
    }

    public Customer(String name, Float the_wallet) {
        this.name = name;
        this.wallet = the_wallet;
    }

    public void refund(Float value) {
        wallet += value;
    }

    public Float getWallet() {
        return wallet;
    }
}
