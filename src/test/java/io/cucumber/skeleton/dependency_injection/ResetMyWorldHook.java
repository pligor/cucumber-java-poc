package io.cucumber.skeleton.dependency_injection;

import cucumber.api.java8.En;
import org.junit.Assert;

public class ResetMyWorldHook implements En {
    //we do not explicitly instantiate the world class
    //the idea is that we have some state in this common class that has been injected to both
    // customer steps class and to goods steps class
    public ResetMyWorldHook() {
        After(new String[]{"@after_reset_world"}, scenario -> {
            MyWorld.getInstance().reset();
        });
    }
}
