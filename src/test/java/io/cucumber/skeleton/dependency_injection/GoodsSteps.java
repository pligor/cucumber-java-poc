package io.cucumber.skeleton.dependency_injection;


import cucumber.api.java8.En;
import org.junit.Assert;

//public class GoodsSteps extends WithWorld implements En {
public class GoodsSteps implements En {
    //we do not explicitly instantiate the world class
    private World world;

    public GoodsSteps(World world) {
//        super();
        this.world = world;

        Given("that {string} bought a faulty {string} for {float} dollars",
                (String name, String item_type, Float value) -> {
                    Assert.assertNull(world.customer);
                    world.customer = new Customer(name);
                    world.item = new Item(item_type, value);
                });
    }
}
