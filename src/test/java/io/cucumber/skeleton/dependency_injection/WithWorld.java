package io.cucumber.skeleton.dependency_injection;

public class WithWorld {
    World world;
    public WithWorld() {
        if(MyWorld.getInstance().world == null) {
            MyWorld.getInstance().world = new World();
        }

        world = MyWorld.getInstance().world;
    }
}

