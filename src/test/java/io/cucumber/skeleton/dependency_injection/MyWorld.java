package io.cucumber.skeleton.dependency_injection;

public class MyWorld {
    public World world;
    private static MyWorld ourInstance = new MyWorld();

    public static MyWorld getInstance() {
        return ourInstance;
    }

    private MyWorld() {
    }

    public void reset() {
        world = null;
    }
}
