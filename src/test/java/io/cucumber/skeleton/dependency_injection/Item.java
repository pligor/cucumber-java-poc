package io.cucumber.skeleton.dependency_injection;

public class Item {
    private String item_type;
    private Float price;

    public Item(String item_type) {
        this.item_type = item_type;
    }

    public Item(String item_type, Float price) {
        this.item_type = item_type;
        this.price = price;
    }

    public String getItem_type() {
        return item_type;
    }

    public Float getPrice() {
        return price;
    }
}
