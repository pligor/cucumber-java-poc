package io.cucumber.skeleton;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

public class Stepdefs {
    @Given("^I have (\\d+) cukes in my belly$")
    public void cukes_in_my_belly(int cukes) throws Throwable {
        Belly belly = new Belly();
        belly.eat(cukes);
    }

    @When("I wait 1 hour")
    public void wait_one_hour() throws Throwable {
        System.out.println("waiting for 1 hour at least dude");
    }

/*    @Then("my belly should growl")
    public void some_step_def() throws Throwable {
        System.out.println("waiting for 1 hour at least dude");
    }*/
}
