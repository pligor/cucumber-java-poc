package io.cucumber.skeleton;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java8.En;
import org.junit.Assert;

public class Stepdefs2 implements En {

    public Stepdefs2() {
        Then("my belly should growl {int} times", (Integer count) -> {
            if (count > 10) {
                Assert.fail("you have put more than enough in this belly");
            }

            System.out.println("growling: " + count);
        });
    }
}
