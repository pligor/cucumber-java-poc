package io.cucumber.skeleton.complex_pages;

import io.cucumber.skeleton.page_object_pattern_package.BasePageObjectRepository;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ComplexPageObjRepo extends BasePageObjectRepository {
    public ComplexPageObjRepo(WebDriver driver) {
        super(driver);
    }

    public WebElement get_first_twitter_button() {
        List<WebElement> twitter_buttons = driver.findElements(By.xpath("//*[contains(@title, 'Twitter')]"));
        return twitter_buttons.get(0);
    }

    public WebElement get_search_field() {
        return driver.findElements(By.cssSelector("input#s")).get(0);
    }

    public WebElement get_search_button() {
        return driver.findElements(By.cssSelector("input#searchsubmit")).get(0);
    }

    public WebElement get_toggle() {
        return driver.findElement(By.cssSelector(".et_pb_toggle_title"));
    }
}
