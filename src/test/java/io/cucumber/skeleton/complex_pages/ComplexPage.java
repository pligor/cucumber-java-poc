package io.cucumber.skeleton.complex_pages;

import io.cucumber.skeleton.page_object_pattern_package.BasePage;
import org.openqa.selenium.WebDriver;

public class ComplexPage extends BasePage<ComplexPageObjRepo> {
    public ComplexPage(WebDriver driver) {
        super(driver, new ComplexPageObjRepo(driver));
    }

    public void go_to() {
        driver.get("http://www.ultimateqa.com/complicated-page/");
    }

    public Boolean is_at() {
        return driver.getCurrentUrl().contains("complicated-page");
    }

    public void click_first_twitter_button() {
        objectRepository.get_first_twitter_button().click();
    }

    public void search(String keyword) {
        objectRepository.get_search_field().sendKeys(keyword);
        objectRepository.get_search_button().click();
    }

    public void click_toggle() {
        objectRepository.get_toggle().click();
    }
}
