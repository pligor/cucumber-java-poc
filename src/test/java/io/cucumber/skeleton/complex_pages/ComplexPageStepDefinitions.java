package io.cucumber.skeleton;

import cucumber.api.Scenario;
import cucumber.api.java8.En;
import cucumber.api.java8.HookBody;
import cucumber.runtime.junit.Assertions;
import io.cucumber.skeleton.complex_pages.ComplexPage;
import io.cucumber.skeleton.page_object_pattern_package.CoursesPage;
import io.cucumber.skeleton.page_object_pattern_package.LoginPage;
import io.cucumber.skeleton.page_object_pattern_package.StudentDashboardPage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class ComplexPageStepDefinitions implements En {
    //The class is being instantiated on every scenario !

    ComplexPage complexPage = null;

    public ComplexPageStepDefinitions() {

        Given("the complicated webpage has been loaded", () -> {
            complexPage = new ComplexPage(MyGlobal.getInstance().driver);
            complexPage.go_to();
            Assert.assertTrue(complexPage.is_at());
        });

        When("clicking the first twitter button", () -> {
           complexPage.click_first_twitter_button();
        });

        Then ("we are no longer in the complex page", () -> {
            Assert.assertFalse(complexPage.is_at());
        });

        When("searching the keyword {string}", (String keyword) -> {
            complexPage.search(keyword);
        });

        When ("clicking to open the toggle", () -> {
           complexPage.click_toggle();
        });
    }


}