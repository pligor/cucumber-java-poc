package io.cucumber.skeleton;

import cucumber.api.java8.En;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CucumberSelenium implements En {
    WebDriver driver;

    public CucumberSelenium() {
        Given("The browser is opened and the application is launched", () -> {
            System.setProperty("webdriver.gecko.driver", "/home/student/home/selenium_drivers/geckodriver_0.24.0_linux_64");
            driver= new FirefoxDriver();
            driver.manage().window().maximize();
            driver.get("http://demo.guru99.com/v4");
        });

        When("username and Password is entered", () -> {
            driver.findElement(By.name("uid")).sendKeys("username12");
            driver.findElement(By.name("password")).sendKeys("password12");
        });


        Then("reset the credentials", () -> {
            driver.findElement(By.name("btnReset")).click();
        });
    }
}
