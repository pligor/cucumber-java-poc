#@after_reset_world
Feature: Refund faulty items
  http://www.thinkcode.se/blog/2017/04/01/sharing-state-between-steps-in-cucumberjvm-using-picocontainer

  Broken items should be refunded if you have receipt

  @di_concept
  Scenario: Returning a broken kettle to the store
    Given that "Joanna" bought a faulty "kettle" for 100.0 dollars
    When she return the kettle to the store
    And she shows her receipt
    Then she will get 100.0 refunded

  @di_concept
  Scenario: Returning a broken kettle to the store
    Given that "Joanna" bought a faulty "kettle" for 100.0 dollars
    When she return the kettle to the store
    And she shows her receipt
    Then she will get 100.0 refunded