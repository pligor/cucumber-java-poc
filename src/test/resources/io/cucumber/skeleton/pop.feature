#mvn test -Dcucumber.options="src/test/resources/io/cucumber/skeleton/pop.feature -t @pop"

Feature: Cucumber with Selenium using the Page Object pattern

  Background:
    Given Selenium Driver has been loaded

  @pop
  Scenario: Trying the page object pattern
    Given the ultimateqa webpage has been loaded
    When clicking on sign in
    And filling out the form with the wrong information and submitting it
    Then an error message is being displayed

  @pop
  @pop_correct
  Scenario: Trying the page object pattern
    Given the ultimateqa webpage has been loaded
    When clicking on sign in
    And filling out and submitting the form
    Then the logged in page has the expected title

  @complex_page_twitter
  Scenario: We are checking how to implement selenium effectively for the case of complex webpages
    Given the complicated webpage has been loaded
    When clicking the first twitter button
    Then we are no longer in the complex page

  @complex_page_search
  Scenario: We are checking the searching
    Given the complicated webpage has been loaded
    When searching the keyword "selenium"
    Then we are no longer in the complex page

#  @After_DriverQuit
  @complex_page
  Scenario: We are checking the toggle
    Given the complicated webpage has been loaded
    When clicking to open the toggle
    Then the content of the toggle should be visible